pysph (1.0~b1-9) UNRELEASED; urgency=medium

  * Standards version bumped to 4.7.0, no changes.
  * debian/patches:
    - New 0009-Setup-requires.patch (Closes: #1083586).
    - New 0010-Fix-syntax-warning.patch (Closes: #1085828).
  * debian/rules:
    - Temporary skip tests in test_nnps.py

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 06 Sep 2024 18:29:38 +0000

pysph (1.0~b1-8) unstable; urgency=medium

  * debian/control:
    - Remove runtime dependenvy form cython (Closes: #1058013).
  * Update dates in d/copyright.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 07 Jan 2024 10:11:47 +0000

pysph (1.0~b1-7) unstable; urgency=medium

  * debian/patches:
    - New 0008-Compatibility-with-cython-v3.x.patch
      (Closes: #1056846).
  * debian/rules:
    - Update the list of tests to skip.
  * debian/control:
    - Drop versioned dependency for python3-mako.
    - Remove sphinxdoc:Depemds from python3-pysph.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 03 Dec 2023 09:26:15 +0000

pysph (1.0~b1-6) unstable; urgency=medium

  * Standards version bumped to 4.6.2, no changes.
  * debian/control:
    - bump debhelper-compat version to 13.
    - use <!nodoc> and <!nocheck> markers.
    - update package description.
  * debian/patches:
    - new 0007-Fix-SyntaxWarning.patch (Closes: #1040292).
  * Update dates in d/copyright.
  * Switch to dh-sequence-*.
  * Improve doc build.
  * New d/clean (Closes: #1047706).
  * debian/rules:
    - Use execute_{before,after} instead of override in rules file.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 17 Aug 2023 08:54:23 +0000

pysph (1.0~b1-5) unstable; urgency=medium

  * debian/rules:
    - enable numpy3 dh helper.
    - disable tests not compatible with numpy v1.24.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 29 Dec 2022 19:26:20 +0000

pysph (1.0~b1-4) unstable; urgency=medium

  * debian/patches:
    - new 0006-Fix-compatibility-with-Python-3.11.patch
      (Closes: #1024498).

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Wed, 23 Nov 2022 17:41:20 +0000

pysph (1.0~b1-3) unstable; urgency=medium

  * debian/patches:
    - new debian/patches/0005-Fix-compatibility-with-mako.patch.
      Closes: #1023028.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 30 Oct 2022 15:03:01 +0000

pysph (1.0~b1-2) unstable; urgency=medium

  * Standards version bumped to 4.6.1, no changes.
  * debian/tests:
    - autopkgtest with all supported python versions.
  * update lintian overrides (to the new format).

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 10 Jul 2022 07:42:53 +0000

pysph (1.0~b1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - remove obsolete dependencies: nose and mock
    - add new dependencies: mayavi, pyopencl, pyside2.
  * Update d/copyright.
  * debian/patches:
    - drop 0005-Spelling.patch and
      0006-Fix-segfaullt-on-non-x86-platforms.patch,
      applied upstream
    - refresh remaining patches.
  * debian/rules:
    - re-enable test_parallel (see
      https://github.com/pypr/pysph/issues/326)
    - fix permissions.
  * Add lintian overrides for
    package-contains-documentation-outside-usr-share-doc.
  * Uodate d/tests scripts.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 11 Mar 2022 07:44:00 +0000

pysph (1.0~b0~20191115.gite3d5e10-6) unstable; urgency=medium

  * Update watch file to use tags instead of releases.
  * Disable broken test (see
    https://github.com/pypr/pysph/issues/326).
    Closes: #997029.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 02 Jan 2022 09:54:54 +0000

pysph (1.0~b0~20191115.gite3d5e10-5) unstable; urgency=medium

  * Standards version bumped to 4.6.0 (no changes).
  * Update the debian/watch file.
  * debian/patches:
    - forward 0005-Spelling.patch.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 12 Sep 2021 10:14:08 +0000

pysph (1.0~b0~20191115.gite3d5e10-4) unstable; urgency=medium

  * Team upload.
  * Upload into unstable

 -- Anton Gladky <gladk@debian.org>  Tue, 09 Feb 2021 19:47:44 +0100

pysph (1.0~b0~20191115.gite3d5e10-4~exp1) experimental; urgency=medium

  * debian/rules:
    - re-enable tests onMultipleLevelsStratified*
  * debian/patches:
    - new 0006-Fix-segfaullt-on-non-x86-platforms.patch
      (Closes: #948736)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 07 Feb 2021 09:38:21 +0000

pysph (1.0~b0~20191115.gite3d5e10-3) unstable; urgency=medium

  * Team upload.
  * Upload into unstable

 -- Anton Gladky <gladk@debian.org>  Mon, 01 Feb 2021 18:09:40 +0100

pysph (1.0~b0~20191115.gite3d5e10-3~exp1) experimental; urgency=medium

  * debian/control:
    - Standards-Version bump ed to 4.5.1 (no change).
  * debian/patches:
    - set the Forwarded tag inall patches
  * debian/rules:
    - temporary skip "MultipleLevelsStratified*" test cases
      (see https://github.com/pypr/pysph/issues/237)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 24 Jan 2021 18:46:07 +0000

pysph (1.0~b0~20191115.gite3d5e10-2) unstable; urgency=medium

  * Standards version bumped to 4.5.0 (no change).
  * Re-enable parallel extension
  * debian/control:
    - build-depend and recommend mpi4py and pyzoltan
    - pysph-viewer recommends h5py and suggests pysph-doc
  * debian/patches:
    - drop 0004-Disable-parallel-extension.patch
    - new 0004-Fix-nprocs.patch: control the number of concurrent
      processes in MPI tests
    - new 0005-Spelling.patch
  * debian/rules:
    - improve clean target

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 19 Apr 2020 16:39:21 +0000

pysph (1.0~b0~20191115.gite3d5e10-1) unstable; urgency=medium

  * New upstream release.
  * Standards version bumped to 4.4.1 (no change).
  * debian/control:
    - update dependencies: added python3-compyle, python3-cyarray
      and python3-beaker
    - recommend python3-pyopencl
    - explicitly specify Rules-Requires-Root
    - require mako >= 1.1.0 (Closes: #945326)
  * No longer install examples (outdated).
  * No longer install pyzoltan (removed upstream).
  * Update debian/copyright.
  * debian/patches:
    - drop 0002-Spelling.patch
    - refresh and renumber remaining patches
    - new 0004-Disable-parallel-extension.patch,
      pyzoltan is not available
  * debian/tests:
    - use the Python 3 version of the package to run autopackagetest
    - improve test scripts

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 04 Jan 2020 11:09:04 +0000

pysph (1.0~a6-3) unstable; urgency=medium

  * debian/rules:
    - fix pytest flags for test exclusion
  * Enable CI on salsa.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 06 Sep 2019 19:19:33 +0000

pysph (1.0~a6-2) unstable; urgency=medium

  * Fix/update debian/copyright file (Closes: #935570).
  * Remove obsolete fields Name from debian/upstream/metadata.
  * Move from experimental to unstable.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 25 Aug 2019 16:36:20 +0000

pysph (1.0~a6-1) experimental; urgency=medium

  * New upstream release.
  * Standards version bumped to 4.4.0 (no change).
  * Drop debian/compat file, and depend on debelper-compat.
  * Set compat to 12.
  * Add upstream metadata file.
  * Add watch file.
  * Fix duplicate files in pysph-doc.
  * Use local mathjax.
  * Register documentation in doc-base.
  * debian/rules:
    - enable hardening flags
    - improve clean target
    - enable testing
  * debian/patches:
    - new 0001-Use-local-mathjax.patch
    - new 0002-Spelling.patch
    - new 0003-autodoc_mock_imports.patch
    - new 0004-Use-Python-3-in-Makefile.patch
  * debian/control:
    - move pysph-viewer to the science section
    - pysph-viewer now is maked as architecture all
    - update depenndencies for new upstream release (pytools)
    - python3-pysph now suggests pysph-doc
    - added dependency on python3-stl
  * Switch to pybuild.
  * Update debian/copyright file.
  * New Python 3 package.
  * Drop Python 2 support.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 18 Aug 2019 17:10:11 +0200

pysph (0~20180411.git1ae58e1-3) unstable; urgency=medium

  * Team upload.
  * [6bd820f] Set Standards-Version to 4.3.0
  * [ae4bb17] Trim trailing whitespace.
  * [bf44b2f] Add Antonio Valentino as an uploader.

 -- Anton Gladky <gladk@debian.org>  Thu, 15 Aug 2019 21:22:06 +0200

pysph (0~20180411.git1ae58e1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add the missing build dependencies on python-pytest
    and python-pytest-runner, thanks to Steve Langasek.
    (Closes: #915514)
  * Update Homepage: to the current upstream location.

 -- Adrian Bunk <bunk@debian.org>  Mon, 07 Jan 2019 23:30:08 +0200

pysph (0~20180411.git1ae58e1-2) unstable; urgency=medium

  * Team upload.
  * [ead1c83] Add python-sphinx-rtd-theme to build-depends

 -- Anton Gladky <gladk@debian.org>  Sun, 20 May 2018 17:43:24 +0200

pysph (0~20180411.git1ae58e1-1) unstable; urgency=medium

  [ Anton Gladky ]
  * Team upload.
  * [7d6d2af] Update VCS fields
  * [5aea85f] Use Standards-Version: 4.1.4
  * [43ebe1d] Use compat-level 11
  * [cc5d3ad] Remove deprecated Testsuite-field in d/control
  * [d19ce32] Update python version number
  * [bd1e06a] Remove deprecated patch
  * [0fbe2df] Update d/rules
  * [c397a0f] Remove x-python-version field
  * [f97cf1a] Remove myself from uploaders
  * [804d0a4] Don not install easy-install.pth (Closes: #884256)
  * [60a358a] New upstream version 0~20180411.git1ae58e1

  [ Chris Lamb ]
  * [7fb9fed] Make the build reproducible. (Closes: #870358)

 -- Anton Gladky <gladk@debian.org>  Sun, 20 May 2018 15:05:01 +0200

pysph (0~20160514.git91867dc-4) unstable; urgency=medium

  * [64fc2c3] Remove redundant sphinx.ext.mathjax. (Closes: #828917)
  * [f5b1301] Minor fix in d/copyright.

 -- Anton Gladky <gladk@debian.org>  Mon, 01 Aug 2016 18:36:18 +0200

pysph (0~20160514.git91867dc-3) unstable; urgency=medium

  * [da8bf51] Allow stderr in autopkgtests.

 -- Anton Gladky <gladk@debian.org>  Sat, 28 May 2016 19:03:04 +0200

pysph (0~20160514.git91867dc-2) unstable; urgency=medium

  * [f4a146c] Add python-dev to depends.

 -- Anton Gladky <gladk@debian.org>  Fri, 27 May 2016 18:36:50 +0200

pysph (0~20160514.git91867dc-1) unstable; urgency=medium

  * [6377614] Add build-essential to Depends.
  * [3ee9544] Imported Upstream version 0~20160514.git91867dc
  * [a7c3909] Refresh patches.
  * [535eeac] Update d/copyright.
  * [dbd40c6] Apply cme fix dpkg.

 -- Anton Gladky <gladk@debian.org>  Thu, 26 May 2016 20:10:04 +0200

pysph (0~20160122.git1fe4786-1) unstable; urgency=medium

  * [725acf5] Imported Upstream version 0~20160122.git1fe4786
  * [a1a3d77] Refresh patches.
  * [456e370] Add python-moko as Depends.
  * [f6238bf] Minor fix in d/rules.
  * [a8f501b] Apply cme fix dpkg.
  * [73d0d1d] Drop site.py. (Closes: #801902)

 -- Anton Gladky <gladk@debian.org>  Tue, 09 Feb 2016 14:36:27 +0100

pysph (0~20150606.gitfa26de9-5) unstable; urgency=medium

  * [ed9bf4e] Add build-essential to depends.

 -- Anton Gladky <gladk@debian.org>  Thu, 18 Jun 2015 20:24:05 +0200

pysph (0~20150606.gitfa26de9-4) unstable; urgency=medium

  * [b3cea9b] Reduce time execution of sphere-autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Tue, 09 Jun 2015 22:26:05 +0200

pysph (0~20150606.gitfa26de9-3) unstable; urgency=medium

  * [ead64b7] Do not install setuptools. (Closes: #788094)
  * [6c3a640] Move mayavi2 in Recommends.
  * [4685482] Fia autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Tue, 09 Jun 2015 21:40:46 +0200

pysph (0~20150606.gitfa26de9-2) unstable; urgency=medium

  * [512783a] Fix binary-arch build. Thanks to Aaron M. Ucko. (Closes: #787967)

 -- Anton Gladky <gladk@debian.org>  Sun, 07 Jun 2015 08:19:22 +0200

pysph (0~20150606.gitfa26de9-1) unstable; urgency=medium

  * [ad3599b] Imported Upstream version 0~20150606.gitfa26de9.
              (Closes: #787573)
  * [54eddc8] Disable using napoleon sphinx module.
  * [1b33dc9] Disable autotest temporary.
  * [e86a545] Install only pysph_viewer in /usr/bin
  * [b9c26dc] Set HOME in /tmp-created dir. (Closes: #787575)
  * [2be6b57] Apply cme fix dpkg-control.

 -- Anton Gladky <gladk@debian.org>  Sun, 07 Jun 2015 00:21:09 +0200

pysph (0~20141130.git9132872-1) unstable; urgency=medium

  * Upload to unstable. (Closes: #772908)

 -- Anton Gladky <gladk@debian.org>  Fri, 12 Dec 2014 06:14:14 +0100
